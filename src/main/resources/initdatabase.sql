create table if not exists project (
	id long identity, 
    name varchar(200), 
	description varchar (200), 
    status varchar (200), 
    start_date varchar (200), 
    end_date varchar (200), 
    budget double (200), 
    current_costs double(200));
    
create table if not exists person (
	id long identity, 
    name varchar(200), 
    firstname varchar (200), 
    role varchar(200));
    
create table if not exists project_person (
	id long identity, 
    person_id integer, 
	project_id integer,
    foreign key (person_id) references person(id) on delete cascade,
    foreign key (project_id) references project(id) on delete cascade);
    
create table if not exists budgetplan (
	id long identity,
	value integer,
	project_id integer,
	foreign key (project_id) references project(id) on delete cascade);
	
create table if not exists milestone (
	id long identity,
	description varchar (200),
	date varchar (200),
	completed integer,
	project_id integer,
	foreign key (project_id) references project(id) on delete cascade);
	
create table if not exists risk (
	id long identity,
	description varchar (200),
	probability integer,
	severity integer,
	rating integer,
	project_id integer,
	foreign key (project_id) references project(id) on delete cascade);
    