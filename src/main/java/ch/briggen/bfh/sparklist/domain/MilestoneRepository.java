package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Meilensteine. 
 * Hier werden alle Funktionen für die DB-Operationen zu den Meilensteinen/Terminen implementiert
 * @author Stefan Burger
 *
 */


public class MilestoneRepository {
	
	private final Logger log = LoggerFactory.getLogger(MilestoneRepository.class);
	
	/**
	 * Liefert alle Einträge in der Datenbank
	 * @return Collection aller Meilensteine
	 */
	public Collection<Milestone> getAll()  {
		log.trace("getAllMilestones");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, date, completed, project_id from milestone");
			ResultSet rs = stmt.executeQuery();
			return mapMilestone(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving milestone. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Einträge einer Projekt ID
	 * @param projectId Gesuchte Projekt ID
	 * @return Collection der Termine eines Projekts
	 */
	public Collection<Milestone> getByProject(long projectId) {
		log.trace("getByProject " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, date, completed, project_id from milestone where project_id=?");
			stmt.setLong(1, projectId);
			ResultSet rs = stmt.executeQuery();
			return mapMilestone(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving milestone by project " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}
	

	/**
	 * Liefert den Meilenstein mit der übergebenen Id
	 * @param id id des Meilenstein
	 * @return Meilenstein
	 */
	public Milestone getById(long id) {
		log.trace("getById " + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, date, completed, project_id from milestone where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapMilestone(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Setzt Termin Completed auf 1 (Zählt als durchgeführt)
	 * @param i
	 */
	public void complete(Milestone m) {
		log.trace("save " + m);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update milestone set completed=1 where id=?");
			stmt.setLong(1, m.getId());
			stmt.executeUpdate();
			
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + m;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den Meilenstein mit der angegebenen Id von der DB
	 * @param id Meilenstein ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from milestone where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing milestone by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert neuen Meilenstein in der DB
	 * @param m neuen Meilenstein
	 * @return Liefert die von der DB generierte id des neuen Meilensteins zurück
	 */
	public long insert(Milestone m) {
		
		log.trace("insert " + m);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into milestone (description, date, completed, project_id) values (?,?,?,?)");
			stmt.setString(1, m.getDescription());
			stmt.setString(2, m.getDate());
			stmt.setInt(3, m.getCompleted());
			stmt.setLong(4, m.getProjectId());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
					
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating milestone " + m;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Meilenstein-Objekten. Siehe getByXXX Methoden.
	 * @author Stefan Burger
	 * @throws SQLException 
	 *
	 */
	private static Collection<Milestone> mapMilestone(ResultSet rs) throws SQLException 
	{
		LinkedList<Milestone> list = new LinkedList<Milestone>();
		while(rs.next())
		{
			Milestone b = new Milestone(rs.getLong("id"),rs.getString("description"),rs.getString("date"),rs.getInt("completed"),rs.getInt("project_id"));
			list.add(b);
		}
		return list;
	}
	
	
}
