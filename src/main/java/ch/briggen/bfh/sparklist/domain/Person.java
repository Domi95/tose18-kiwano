package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag einer Person
 * @author Dominique Bachmann
 *
 */
public class Person {
	
	private long id;
	private String name;
	private String firstname;
	private String role;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Person()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name der Person
	 * @param firstname Vorname der Person
	 * @param role Rolle der Person
	 */
	
	public Person(long id, String name, String firstname, String role)
	{
		this.id = id;
		this.name = name;
		this.firstname = firstname;
		this.role = role;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname= firstname;
	}

	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return String.format("Person:{id: %d; name: %s; firstname: %s; role: %s}", id, name, firstname, role);
	}
	

}
