package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelne Projekt-Person Beziehung
 * @author Stefan Burger
 *
 */
public class ProjectPerson {
	
	private long id;
	private int projectId;
	private int personId;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public ProjectPerson()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param projectId Projekt ID
	 * @param personId Person ID
	 */
	
	public ProjectPerson(long id, int projectId, int personId)
	{
		this.id = id;
		this.projectId = projectId;
		this.personId = personId;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}
	
	@Override
	public String toString() {
		return String.format("ProjectPerson:{id: %d; projectId: %i; personId: %i}", id, projectId, personId);
	}
	

}