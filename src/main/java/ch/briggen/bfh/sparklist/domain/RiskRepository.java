package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Risiken. 
 * Hier werden alle Funktionen für die DB-Operationen zu den Risiken implementiert
 * @author Stefan Burger
 *
 */


public class RiskRepository {
	
	private final Logger log = LoggerFactory.getLogger(RiskRepository.class);
	
	/**
	 * Liefert alle Einträge in der Datenbank
	 * @return Collection aller Risiken
	 */
	public Collection<Risk> getAll()  {
		log.trace("getAllRisks");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, probability, severity, rating, project_id from risk");
			ResultSet rs = stmt.executeQuery();
			return mapRisk(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving risks. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Einträge einer Projekt ID
	 * @param projectId Gesuchte Projekt ID
	 * @return Collection der Termine eines Projekts
	 */
	public Collection<Risk> getByProject(long projectId) {
		log.trace("getByProject " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, probability, severity, rating, project_id from risk where project_id=?");
			stmt.setLong(1, projectId);
			ResultSet rs = stmt.executeQuery();
			return mapRisk(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving risk by project " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}
	

	/**
	 * Liefert das Risiko mit der übergebenen Id
	 * @param id id des Meilenstein
	 * @return Meilenstein
	 */
	public Risk getById(long id) {
		log.trace("getById " + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, description, probability, severity, rating, project_id from risk where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapRisk(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving risk by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Löscht das Risiko mit der angegebenen Id von der DB
	 * @param id Risk ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from risk where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting risk by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
				
	}

	/**
	 * Speichert neues Risiko in der DB
	 * @param r neues Risiko
	 * @return Liefert die von der DB generierte id des neuen Risikos zurück
	 */
	public long insert(Risk r) {
		
		log.trace("insert " + r);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into risk (description, probability, severity, rating, project_id) values (?,?,?,?,?)");
			stmt.setString(1, r.getDescription());
			stmt.setInt(2, r.getProbability());
			stmt.setInt(3, r.getSeverity());
			stmt.setInt(4, r.getRating());
			stmt.setLong(5, r.getProjectId());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
					
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating risk " + r;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Risk-Objekten. Siehe getByXXX Methoden.
	 * @author Stefan Burger
	 * @throws SQLException 
	 *
	 */
	private static Collection<Risk> mapRisk(ResultSet rs) throws SQLException 
	{
		LinkedList<Risk> list = new LinkedList<Risk>();
		while(rs.next())
		{
			Risk r = new Risk(rs.getLong("id"),rs.getString("description"),rs.getInt("probability"),rs.getInt("severity"),rs.getInt("rating"),rs.getInt("project_id"));
			list.add(r);
		}
		return list;
	}
	
	
}
