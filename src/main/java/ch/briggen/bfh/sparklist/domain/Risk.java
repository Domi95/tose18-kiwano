package ch.briggen.bfh.sparklist.domain;

/**
 * Risk (Repräsentiert Risikoplanung)
 * @author Stefan Burger
 *
 */
public class Risk {
	
	private long id;
	private String description;
	private int probability;
	private int severity;
	private int rating;
	private long projectId;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Risk()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param description Beschreibung
	 * @param probability Eintrittswahrscheinlichkeit
	 * @param severity Schadensausmass
	 * @param rating Risikograd
	 * @param projectId Zugehörige Projekt ID
	 */
	
	public Risk(long id, String description, int probability, int severity, int rating, int projectId)
	{
		this.id = id;
		this.description = description;
		this.probability = probability;
		this.severity = severity;
		this.rating = rating;
		this.projectId = projectId;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getProbability() {
		return probability;
	}

	public void setProbability(int probability) {
		this.probability = probability;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId= projectId;
	}
	
	@Override
	public String toString() {
		return String.format("Budget:{id: %d; description: %s; probability: %d; severity: %d; rating: %d projectId: %d}", id, description, probability, severity, rating, projectId);
	}

	

}
