package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Einträge im Budgetplan. 
 * Hier werden alle Funktionen für die DB-Operationen zum Budget implementiert
 * @author Stefan Burger
 *
 */


public class BudgetRepository {
	
	private final Logger log = LoggerFactory.getLogger(BudgetRepository.class);
	
	/**
	 * Liefert alle Einträge in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Budget> getAll()  {
		log.trace("getByProject");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, value, project_id from budgetplan");
			ResultSet rs = stmt.executeQuery();
			return mapBudget(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving budgetplan. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Einträge eines bestimmten Projekts
	 * @param projectId Ausgewähltes Projekt
	 * @return Collection der Einträge eines bestimmten Projekts
	 */
	public Collection<Budget> getByProject(long projectId) {
		log.trace("getByProject " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, value, project_id from budgetplan where project_id=?");
			stmt.setLong(1, projectId);
			ResultSet rs = stmt.executeQuery();
			return mapBudget(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving budget by project " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}
	

	/**
	 * Liefert den Eintrag mit der übergebenen Id
	 * @param id id des Eintrags
	 * @return Budgeteintrag
	 */
	public Budget getById(long id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, value, project_id from budgetplan where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapBudget(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Löscht den Eintrag mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from budgetplan where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing budget by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert einen neuen Eintrag in der DB. INSERT.
	 * @param b neuen Eintrag
	 * @return Liefert neu generierte ID
	 */
	public long insert(Budget b) {
		
		log.trace("insert " + b);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into budgetplan (value, project_id) values (?,?)");
			stmt.setInt(1, b.getValue());
			stmt.setLong(2, b.getProjectId());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
					
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating budget " + b;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Budget-Objekten. Siehe getByXXX Methoden.
	 * @author Stefan Burger
	 * @throws SQLException 
	 *
	 */
	private static Collection<Budget> mapBudget(ResultSet rs) throws SQLException 
	{
		LinkedList<Budget> list = new LinkedList<Budget>();
		while(rs.next())
		{
			Budget b = new Budget(rs.getLong("id"),rs.getInt("value"),rs.getInt("project_id"));
			list.add(b);
		}
		return list;
	}
	
	
}
