package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projekte. 
 * Hier werden alle Funktionen für die DB-Operationen zu den Projekten implementiert
 * @author Stefan Burger
 *
 */


public class ProjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
	private static Map<Long, ArrayList<Long>> personProjects;
	
	public ProjectRepository() {
		personProjects = mapProjectPerson();
		
	}
	

	/**
	 * Liefert alle Projekte aus der Datenbank
	 * @return Collection aller Projekte
	 */
	public Collection<Project> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, description, start_date, end_date, budget, current_costs, status from project");
			ResultSet rs = stmt.executeQuery();
			return mapProject(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projekte mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Project> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, description, start_date, end_date, budget, current_costs, status from project where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapProject(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Projekt mit der übergebenen Id
	 * @param id id des Item
	 * @return Projekt
	 */
	public Project getById(long id) {
		log.trace("getById " + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, description, start_date, end_date, budget, current_costs, status from project where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			
			//TODO: Auslesen der Projekt-Personen Hier
			
			
			
			return mapProject(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Projekt in der Datenbank. UPDATE.
	 * For-Schleife prüft und fügt Beziehungen ein
	 * @param i
	 */
	public void save(Project p) {
		log.trace("save " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update project set name=?, description=?, start_date=?, end_date=?, budget=?, current_costs=?, status=? where id=?");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getDescription());
			stmt.setString(3, p.getStartDate());
			stmt.setString(4, p.getEndDate());
			stmt.setDouble(5, p.getBudget());
			stmt.setDouble(6, p.getCurrentCosts());
			stmt.setString(7, p.getStatus());
			stmt.setLong(8, p.getId());
			stmt.executeUpdate();
			
			
			List<Long> projectIds = p.getPersons();
	        
	        for (int i = 0; i < projectIds.size(); i++) {
	          stmt = conn.prepareStatement("insert into project_person (person_id, project_id) values (?, ?)");  
	          stmt.setLong(2, p.getId());        
	          stmt.setLong(1, projectIds.get(i));
	          stmt.executeUpdate();  
	        }
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}
	
	public void updateCostsFromBudget(Project p, int costs) {
		log.trace("save " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update project set current_costs=? where id=?");
			stmt.setDouble(1, costs);
			stmt.setLong(2, p.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Projekt mit der angegebenen Id von der DB
	 * @param id Projekt ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from project where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
	
	/**
	 * Löscht die Beziehung mit der angegebenen Id von der DB
	 * @param id Project_Person ID
	 */
	public void deleteRelation(long projectId, long personId) {
		log.trace("delete " + projectId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from project_person where project_id=? and person_id=?");
			stmt.setLong(1, projectId);
			stmt.setLong(2, personId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing project by id " + projectId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Projekt in der DB. INSERT.
	 * @param p neu zu erstellendes Projekt
	 * @return Liefert die von der DB generierte id des neuen Projekts zurück
	 */
	public long insert(Project p) {
		
		log.trace("insert " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into project (name, description, start_date, end_date, budget, current_costs, status) values (?,?,?,?,?,?,?)");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getDescription());
			stmt.setString(3, p.getStartDate());
			stmt.setString(4, p.getEndDate());
			stmt.setDouble(5, p.getBudget());
			stmt.setDouble(6, p.getCurrentCosts());
			stmt.setString(7, p.getStatus());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			
			List<Long> projectIds = p.getPersons();
	        
	        for (int i = 0; i < projectIds.size(); i++) {
	          stmt = conn.prepareStatement("insert into project_person (person_id, project_id) values (?, ?)");  
	          stmt.setLong(2, id);        
	          stmt.setLong(1, projectIds.get(i));
	          stmt.executeUpdate();  
	        }
			
			
			
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Projekt-Objekte. Siehe getByXXX Methoden.
	 * @author Stefan Burger
	 * @throws SQLException 
	 *
	 */
	private static Collection<Project> mapProject(ResultSet rs) throws SQLException 
	{
		LinkedList<Project> list = new LinkedList<Project>();
		while(rs.next())
		{
			ArrayList<Long> persons = getPersons(rs.getLong("id"));
			Project p = new Project(
					rs.getLong("id"),
					rs.getString("name"),
					rs.getString("description"),
					rs.getString("start_date"),
					rs.getString("end_date"),
					rs.getDouble("budget"),
					rs.getDouble("current_costs"),
					rs.getString("status"),
					persons);
			list.add(p);
		}
		return list;
	}
	
	/**
	 * Liefert Liste der Personen eines Projekts
	 * @param projectId Projekt ID
	 * @return Liste der Personen
	 */
    private static ArrayList<Long> getPersons(long projectId) {
      if (personProjects.containsKey(projectId)) {
        return personProjects.get(projectId);
      } else {
        return new ArrayList<Long>();
      }
    }

	/**
	 * Liest alle Beziehungen aus der DB und mappt diese
	 * @return Liefert Map der Personen und deren Projekt ID
	 */
    private Map<Long, ArrayList<Long>> mapProjectPerson() {
      try(Connection conn = getConnection()) {
	        PreparedStatement stmt = conn.prepareStatement("select * from project_person");
	        ResultSet rs = stmt.executeQuery();
	        
	        Map<Long, ArrayList<Long>> personProjectMap = new HashMap<>();
	        while(rs.next()) {
		          Long pid = rs.getLong("person_id");
		          ArrayList<Long> projectIds = personProjectMap.get(pid);
		          
		          if (projectIds == null) {
		            projectIds = new ArrayList<Long>();
		          }
		          projectIds.add(rs.getLong("project_id"));
		          personProjectMap.put(pid, projectIds);
	        }
	        return personProjectMap;
      }
      catch(SQLException e) {
	        String msg = "SQL error while getting project_person";
	        log.error(msg , e);
	        throw new RepositoryException(msg);
      }
    }
	
	
}
