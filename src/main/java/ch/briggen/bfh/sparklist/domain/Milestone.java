package ch.briggen.bfh.sparklist.domain;

/**
 * Milestone (Repräsentiert Terminplan)
 * @author Stefan Burger
 *
 */
public class Milestone {
	
	private long id;
	private String description;
	private String date;
	private int completed;
	private long projectId;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Milestone()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Eintrags in der Liste
	 * @param description Beschreibung
	 * @param status Projektstatus
	 * @param projectId Zugehörige Projekt ID
	 */
	
	public Milestone(long id, String description, String date, int completed, int projectId)
	{
		this.id = id;
		this.description = description;
		this.date = date;
		this.completed = completed;
		this.projectId = projectId;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public Integer getCompleted() {
		return completed;
	}
	
	public void setCompleted(Integer completed) {
		this.completed = completed;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId= projectId;
	}
	
	@Override
	public String toString() {
		return String.format("Budget:{id: %d; date: %s; description: %s; projectId: %d}", id, date, description, projectId);
	}

	

}
