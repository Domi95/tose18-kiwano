package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Personen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Personen implementiert
 * @author Dominique Bachmann
 *
 */


public class PersonRepository {
	
	private final Logger log = LoggerFactory.getLogger(PersonRepository.class);
	

	/**
	 * Liefert alle Personen in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Person> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, firstname, role from person order by role");
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all person. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Personen mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Person> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, firstname, role from person where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving person by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert die Person mit der übergebenen Id
	 * @param id id der Person
	 * @return Person
	 */
	public Person getById(long id) {
		log.trace("getById " + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, firstname, role from person where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving person by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	/**
	 * Liefert die Personen mit der übergebenen Projekt Id
	 * @param id id der Person
	 * @return Person
	 */
	public Collection<Person> getByProject(long id) {
		log.trace("getByProjectId " + id);
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pe.id, pe.name, firstname, role from person pe join project_person pp on pp.person_id = pe.id join project pr on pr.id = pp.project_id and pr.id in (?) group by pe.id order by role");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs);	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving person by Relation " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert die übergebene Person in der DB. UPDATE.
	 * @param p
	 */
	public void save(Person p) {
		log.trace("save " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update person set name=?, firstname=?, role=? where id=?");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getFirstname());
			stmt.setString(3, p.getRole());
			stmt.setLong(4, p.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating person " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Person mit der angegebenen ID aus der DB
	 * @param id Person ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from person where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing person by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert die neue Person in der DB. INSERT.
	 * @param p neu zu erstellende Person
	 * @return Liefert die von der DB generierte id der neuen Person
	 */
	public long insert(Person p) {
		
		log.trace("insert " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into person (name, firstname, role) values (?,?,?)");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getFirstname());
			stmt.setString(3, p.getRole());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating person " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Person-Objekten. Siehe getByXXX Methoden.
	 * @throws SQLException 
	 *
	 */
	private static Collection<Person> mapPerson(ResultSet rs) throws SQLException 
	{
		LinkedList<Person> list = new LinkedList<Person>();
		while(rs.next())
		{
			Person p = new Person(rs.getLong("id"),rs.getString("name"),rs.getString("firstname"),rs.getString("role"));
			list.add(p);
		}
		return list;
	}

}
