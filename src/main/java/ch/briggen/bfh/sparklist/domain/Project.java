package ch.briggen.bfh.sparklist.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Stefan Burger
 *
 */
public class Project {
	
	private long id;
	private String name;
	private String description;
	private String status;
	private String startdate;
	private String enddate;
	private double budget;
	private double currentcosts;
	private List<Long> persons = new ArrayList<>();
	
	//Live-Variablen
	private int allMilestones;
	private int completedMilestones;
	private int allRisks;
	private int highRisks;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Project()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Eintrags in der Liste
	 * @param description Beschreibung
	 * @param startdate Startdatum
	 * @param enddate Enddatum
	 * @param budget Gesamtbudget
	 * @param currentcosts Aktuelle Kosten
	 * @param status Projektstatus
	 * @param persons Liste der Personen
	 */
	
	public Project(long id, String name, String description, String startdate, String enddate, double budget, double currentcosts, String status, ArrayList<Long> persons)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.startdate = startdate;
		this.enddate = enddate;
		this.budget = budget;
		this.currentcosts = currentcosts;
		this.status = status;
		this.persons = persons;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStartDate() {
		return startdate;
	}
	
	public void setStartDate(String startdate) {
		this.startdate = startdate;
	}
	
	public String getEndDate() {
		return enddate;
	}
	
	public void setEndDate(String enddate) {
		this.enddate = enddate;
	}
	
	public double getBudget() {
		return budget;
	}
	
	public void setBudget(double budget) {
		this.budget = budget;
	}
	
	public double getCurrentCosts() {
		return currentcosts;
	}
	
	public void setCurrentCosts(double currentcosts) {
		this.currentcosts = currentcosts;
	}
	
	
	 public List<Long> getPersons() {
	 
		return persons;
	}
	
	public void setPersons(List<Long> persons) {
		this.persons = persons;
	}
	

	public int getAllMilestones() {
		return allMilestones;
	}

	public void setAllMilestones(int allMilestones) {
		this.allMilestones = allMilestones;
	}

	public int getCompletedMilestones() {
		return completedMilestones;
	}

	public void setCompletedMilestones(int completedMilestones) {
		this.completedMilestones = completedMilestones;
	}
	
	public int getAllRisks() {
		return allRisks;
	}

	public void setAllRisks(int allRisks) {
		this.allRisks = allRisks;
	}
	
	public int getHighRisks() {
		return highRisks;
	}

	public void setHighRisks(int highRisks) {
		this.highRisks = highRisks;
	}
	
	@Override
	public String toString() {
		return String.format("Project:{id: %d; name: %s; description: %s; startdate: %s; enddate %s; budget %s; currentcosts %s; status: %s}", id, name, description, startdate, enddate, budget, currentcosts, status);
	}





	

}
