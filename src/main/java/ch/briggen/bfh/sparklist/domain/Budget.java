package ch.briggen.bfh.sparklist.domain;

/**
 * Budget (Repräsentiert einen Eintrag im Budgetplan)
 * @author Stefan Burger
 *
 */
public class Budget {
	
	private long id;
	private int value;
	private long projectId;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Budget()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param value Wert des Eintrags
	 * @param projectId Zugehöriges Projekt
	 */
	
	public Budget(long id, int value, int projectId)
	{
		this.id = id;
		this.value = value;
		this.projectId = projectId;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId= projectId;
	}
	
	@Override
	public String toString() {
		return String.format("Budget:{id: %d; value: %d; projectId: %d}", id, value, projectId);
	}
	

}
