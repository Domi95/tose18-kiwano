package ch.briggen.bfh.sparklist;

/**
 * @author Stefan Burger
 *
 */

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectPersonDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.ReportListController;
import ch.briggen.bfh.sparklist.web.RiskDeleteController;
import ch.briggen.bfh.sparklist.web.RiskListController;
import ch.briggen.bfh.sparklist.web.RiskNewController;
import ch.briggen.bfh.sparklist.web.BudgetDeleteController;
import ch.briggen.bfh.sparklist.web.BudgetListController;
import ch.briggen.bfh.sparklist.web.BudgetNewController;
import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.MilestoneCompleteController;
import ch.briggen.bfh.sparklist.web.MilestoneDeleteController;
import ch.briggen.bfh.sparklist.web.MilestoneListController;
import ch.briggen.bfh.sparklist.web.MilestoneNewController;
import ch.briggen.bfh.sparklist.web.PersonDeleteController;
import ch.briggen.bfh.sparklist.web.PersonEditController;
import ch.briggen.bfh.sparklist.web.PersonListController;
import ch.briggen.bfh.sparklist.web.PersonNewController;
import ch.briggen.bfh.sparklist.web.PersonUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/project", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/project/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		get("/project/person/delete", new ProjectPersonDeleteController(), new UTF8ThymeleafTemplateEngine());
		
		get("/persons", new PersonListController(), new UTF8ThymeleafTemplateEngine());
		get("/persons/person", new PersonEditController(), new UTF8ThymeleafTemplateEngine());
		post("/persons/person/update", new PersonUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/persons/person/delete", new PersonDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/persons/person/new", new PersonNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/reports", new ReportListController(), new UTF8ThymeleafTemplateEngine());
		
		get("/reports/budget/project", new BudgetListController(), new UTF8ThymeleafTemplateEngine());
		get("/reports/budget/project/delete", new BudgetDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("reports/budget/project/new", new BudgetNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/reports/milestone/project", new MilestoneListController(), new UTF8ThymeleafTemplateEngine());
		get("/reports/milestone/project/delete", new MilestoneDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("reports/milestone/project/new", new MilestoneNewController(), new UTF8ThymeleafTemplateEngine());
		get("reports/milestone/project/complete", new MilestoneCompleteController(), new UTF8ThymeleafTemplateEngine());
		
		get("/reports/risk/project", new RiskListController(), new UTF8ThymeleafTemplateEngine());
		get("/reports/risk/project/delete", new RiskDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("reports/risk/project/new", new RiskNewController(), new UTF8ThymeleafTemplateEngine());
		

	}

}
