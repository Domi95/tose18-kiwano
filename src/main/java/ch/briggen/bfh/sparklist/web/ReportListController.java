package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.MilestoneRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Risk;
import ch.briggen.bfh.sparklist.domain.RiskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Liefert unter "/reports" die ganze Reportliste
 * 
 * @author Stefan Burger
 *
 */
public class ReportListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ReportListController.class);

	ProjectRepository repository = new ProjectRepository();
	MilestoneRepository mrep = new MilestoneRepository();
	RiskRepository rrep = new RiskRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Projects werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		Collection<Project> allProjects = repository.getAll();
		
		for (Project p : allProjects) {
			Collection<Milestone> cm = mrep.getByProject(p.getId());
			Collection<Risk> cr = rrep.getByProject(p.getId());
			
			//Live-Variablen für View
			Integer allMilestones = 0;
			Integer completedMilestones = 0;
			Integer allRisks = 0;
			Integer highRisks = 0;
			
			//Meilensteine zum Vergleich mit abgeschlossenen
			for (Milestone m : cm) {
	        	allMilestones++;
	        	if (m.getCompleted() > 0) {
	        		completedMilestones ++;
	        	}
	        }
			
			//Risiken zum Vergleich mit kritischen Risiken
			for (Risk r : cr) {
				allRisks++;
				if (r.getRating() == 3) {
					highRisks++;
				}
			}
			p.setAllRisks(allRisks);
			p.setHighRisks(highRisks);
			p.setAllMilestones(allMilestones);
			p.setCompletedMilestones(completedMilestones);
		}
		
		model.put("list", allProjects);
		return new ModelAndView(model, "reportListTemplate");
	}
}
