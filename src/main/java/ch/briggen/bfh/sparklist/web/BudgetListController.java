package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Liefert die ganze Budgetliste per Projekt
 * @author Stefan Burger
 *
 */
public class BudgetListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(BudgetListController.class);

	BudgetRepository budgetRepo = new BudgetRepository();
	ProjectRepository projectRepo = new ProjectRepository();

	/**
	 *Liefert die Liste zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		//Budgetplan wird geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		
		String idString = request.queryParams("id");
		Project p = projectRepo.getById(Integer.parseInt(idString));
		Collection<Budget> cb = budgetRepo.getByProject(Long.parseLong(idString));
		Integer fullCosts = 0;
		
		//Addiert einzelne Budgetwerte zum Gesamtbudgetplan
		for (Budget b : cb) {
	        	fullCosts += b.getValue();
	        }
		
		
		model.put("list", cb);
		model.put("activeProject", p);
		model.put("fullCosts", fullCosts);
		model.put("budgetList", budgetRepo.getAll());

			log.trace("GET Budgetplan für /project mit id " + idString);
			model.put("postAction", "/reports/budget/project/new");
			model.put("budgetDetail", new Budget());
		
		
		return new ModelAndView(model, "budgetListTemplate");
	}
}
