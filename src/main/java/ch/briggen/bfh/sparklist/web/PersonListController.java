package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert Liste der Peronen
 * 
 * @author S.Burger
 *
 */
public class PersonListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(PersonListController.class);

	PersonRepository repository = new PersonRepository();

	/**
	 *Liefert die Liste als person-Seite "/persons" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Persons werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Collection<Person>> model = new HashMap<String, Collection<Person>>();
		model.put("list", repository.getAll());
		return new ModelAndView(model, "personListTemplate");
	}
}
