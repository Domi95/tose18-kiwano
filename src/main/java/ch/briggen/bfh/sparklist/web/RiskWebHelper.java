package ch.briggen.bfh.sparklist.web;

/**
 * Beinhaltet Parameter für Risiko-Objekt in der View
 * 
 * @author Dominique Bachmann
 *
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.Risk;
import spark.Request;
import java.util.List;

class RiskWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(RiskWebHelper.class);
	
	public static Risk riskFromWeb(Request request)
	{
		return new Risk(
				Long.parseLong(request.queryParams("riskDetail.id")),
				request.queryParams("riskDetail.description"),
				Integer.parseInt(request.queryParams("riskDetail.probability")),
				Integer.parseInt(request.queryParams("riskDetail.severity")),
				Integer.parseInt(request.queryParams("riskDetail.rating")),
				Integer.parseInt(request.queryParams("riskDetail.project_id")));
	}

}
