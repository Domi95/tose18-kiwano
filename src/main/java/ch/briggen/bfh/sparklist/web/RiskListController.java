package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Risk;
import ch.briggen.bfh.sparklist.domain.RiskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Liefert die Liste für alle Risiken eines Projekts
 * 
 * @author Stefan Burger
 *
 */
public class RiskListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(RiskListController.class);

	RiskRepository riskRepo = new RiskRepository();
	ProjectRepository prep = new ProjectRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		//Risiken werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		
		String idString = request.queryParams("id");
		Project p = prep.getById(Integer.parseInt(idString));
		Collection<Risk> cr = riskRepo.getByProject(Integer.parseInt(idString));
		Integer allRisks = 0;
		Integer highRisks = 0;
		
		for (Risk r : cr) {
	        	allRisks++;
	        	if (r.getRating() == 3) {
					highRisks++;
				}
	        }
		
		
		model.put("list", cr);
		model.put("activeProject", p);
		model.put("allRisks", allRisks);
		model.put("highRisks", highRisks);
		model.put("riskList", riskRepo.getAll());

			log.trace("GET Risk für /risk mit id " + idString);
			//der Submit-Button ruft /project/new auf --> INSERT
			model.put("postAction", "/reports/risk/project/new");
			model.put("riskDetail", new Risk());
		
		
		return new ModelAndView(model, "riskListTemplate");
	}
}
