package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für delete-Operation für Personen
 * 
 * @author Dominique Bachmann
 *
 */

public class PersonDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonDeleteController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	

	/**
	 * Löscht die Person mit der übergebenen id in der Datenbank	 
	 * Hört auf GET /persons/person/delete
	 * 
	 * @return Redirect zurück zur Personenliste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /persons/person/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		personRepo.delete(longId);
		response.redirect("/persons");
		return null;
	}
}


