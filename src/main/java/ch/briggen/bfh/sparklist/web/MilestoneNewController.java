package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.MilestoneRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für new-Operation in der Meilensteinliste
 * 
 * @author Dominique Bachmann
 *
 */

public class MilestoneNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MilestoneNewController.class);
		
	private MilestoneRepository milestoneRepo = new MilestoneRepository();
	
	/**
	 * Erstellt einen neuen Meilenstein in der DB. Die id wird von der Datenbank erstellt.
	 * Hört auf POST /reports/milestone/project/new
	 * 
	 * @return Redirect zur Meilensteinliste (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Milestone milestoneDetail = MilestoneWebHelper.budgetFromWeb(request);
		log.trace("POST /reports/milestone/project/new mit milestoneDetail " + milestoneDetail);
		
		long id = milestoneRepo.insert(milestoneDetail);
		long pid = milestoneDetail.getProjectId();
		
		response.redirect("/reports/milestone/project?id="+pid);
		return null;
	}
}


