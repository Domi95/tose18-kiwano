package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.MilestoneRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für complete-Operation
 * 
 * @author Dominique Bachmann
 *
 */

public class MilestoneCompleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MilestoneCompleteController.class);
		
	private MilestoneRepository milestoneRepo = new MilestoneRepository();
	

	/**
	 * Schliesst Meilenstein mit übergebenen ID ab	 
	 * Hört auf GET /reports/milestone/project/complete
	 * 
	 * @return Redirect zur Meilensteinliste (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /reports/milestone/project/complete mit id " + id);
		
		Long longId = Long.parseLong(id);
		
		Milestone m = milestoneRepo.getById(longId);
		long pid = m.getProjectId();
		
		//Setzt completed in Meilenstein auf 1 (Abgeschlossen)
		milestoneRepo.complete(m);
		response.redirect("/reports/milestone/project?id="+pid);
		
		
		return null;
	}
}


