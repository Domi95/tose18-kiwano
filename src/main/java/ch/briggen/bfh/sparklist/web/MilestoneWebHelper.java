package ch.briggen.bfh.sparklist.web;

/**
 * Beinhalted Parameter für Meilenstein-Objekt in der View
 * 
 * @author Dominique Bachmann
 *
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import spark.Request;

class MilestoneWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MilestoneWebHelper.class);
	
	public static Milestone budgetFromWeb(Request request)
	{
		return new Milestone(
				Long.parseLong(request.queryParams("milestoneDetail.id")),
				request.queryParams("milestoneDetail.description"),
				request.queryParams("milestoneDetail.date"),
				Integer.parseInt(request.queryParams("milestoneDetail.completed")),
				Integer.parseInt(request.queryParams("milestoneDetail.project_id")));
	}

}
