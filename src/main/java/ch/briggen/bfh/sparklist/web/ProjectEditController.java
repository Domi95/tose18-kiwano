package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für edit-Operationen auf einzelne Projekte

 * @author Stefan Burger
 *
 */

public class ProjectEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjectEditController.class);
	
	
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private PersonRepository personRepo = new PersonRepository();



	private ArrayList<Person> removable;
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Projekts. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /project/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Projekt mit der übergebenen id upgedated (Aufruf /project/save)
	 * Hört auf GET /project
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "projectDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		Collection <Person> pc = personRepo.getAll();
				
		if(null == idString)
		{
			log.trace("GET /project für INSERT mit id " + idString);
			//der Submit-Button ruft /project/new auf --> INSERT
			model.put("postAction", "/project/new");
			model.put("projectDetail", new Project());
			model.put("personListByProject", null);
			model.put("personList", pc);

		}
		else
		{
			Collection <Person> pcByProject = personRepo.getByProject(Long.parseLong(idString));
			//Entfernt bereits hinzugefügte Personen aus der hinzufügbaren Liste
			removable = new ArrayList<Person>();
			for (Person p : pc) {
				for (Person pbp : pcByProject) {
					if (p.getId() == pbp.getId()) {
						removable.add(p);
					}
				}
			}
			pc.removeAll(removable);
			model.put("personListByProject", pcByProject);
			model.put("personList", pc);
			
			
			
			log.trace("GET /project für UPDATE mit id " + idString);
			//der Submit-Button ruft /project/update auf --> UPDATE
			model.put("postAction", "/project/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Project p = projectRepo.getById(id);
			model.put("projectDetail", p);
		}
		
		//das Template projectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projectDetailTemplate");
	}
	
	
	
}


