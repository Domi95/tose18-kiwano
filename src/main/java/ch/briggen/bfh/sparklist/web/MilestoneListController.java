package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.MilestoneRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Liefert die Meilensteinliste je Projekt
 * 
 * @author Stefan Burger
 *
 */
public class MilestoneListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(MilestoneListController.class);

	MilestoneRepository milestoneRepo = new MilestoneRepository();
	ProjectRepository projectRepo = new ProjectRepository();

	/**
	 *Liefert die Liste zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		//Meilensteine werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		
		String idString = request.queryParams("id");
		Project p = projectRepo.getById(Integer.parseInt(idString));
		Collection<Milestone> cm = milestoneRepo.getByProject(Integer.parseInt(idString));
		Integer allMilestones = 0;
		Integer completedMilestones = 0;
		
		//Kontrolliert Anzahl aller und abgeschlossener Meilensteine
		for (Milestone m : cm) {
	        	allMilestones++;
	        	if (m.getCompleted() > 0) {
	        		completedMilestones++;
	        	}
	        }
		
		
		model.put("list", cm);
		model.put("activeProject", p);
		model.put("allMilestones", allMilestones);
		model.put("completedMilestones", completedMilestones);
		model.put("miletsoneList", milestoneRepo.getAll());

			log.trace("GET Milestone für /project mit id " + idString);
			model.put("postAction", "/reports/milestone/project/new");
			model.put("milestoneDetail", new Milestone());
		
		
		return new ModelAndView(model, "milestoneListTemplate");
	}
}
