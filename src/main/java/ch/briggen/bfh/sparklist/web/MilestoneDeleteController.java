package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Milestone;
import ch.briggen.bfh.sparklist.domain.MilestoneRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für delete-Operationen in der Meilensteinliste
 *
 * @author Dominique Bachmann
 *
 */

public class MilestoneDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MilestoneDeleteController.class);
		
	private MilestoneRepository milestoneRepo = new MilestoneRepository();
	

	/**
	 * Löscht den Meilenstein mit der übergebenen id in der Datenbank
	 * Hört auf GET /reports/milestone/project/delete
	 * 
	 * @return Redirect zur Meilensteinliste (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /reports/milestone/project/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		
		Milestone m = milestoneRepo.getById(longId);
		long pid = m.getProjectId();
		
		response.redirect("/reports/milestone/project?id="+pid);
		milestoneRepo.delete(longId);
		
		return null;
	}
}


