package ch.briggen.bfh.sparklist.web;

/**
 * Beinhaltet Parameter für Budget-Objekt in der View
 * 
 * @author Dominique Bachmann
 *
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import spark.Request;

class BudgetWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(BudgetWebHelper.class);
	
	public static Budget budgetFromWeb(Request request)
	{
		return new Budget(
				Long.parseLong(request.queryParams("budgetDetail.id")),
				Integer.parseInt(request.queryParams("budgetDetail.value")),
				Integer.parseInt(request.queryParams("budgetDetail.project_id")));
	}

}
