package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für update-Operationen auf einzelnen Personen

 * @author Stefan Burger
 *
 */

public class PersonUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(PersonUpdateController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	


	/**
	 * Schreibt die geänderte Person zurück in die Datenbank
	 * 
	 * Hört auf POST /persons/person/update
	 * 
	 * @return redirect zur Personenliste
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		
		log.trace("POST /persons/person/update mit personDetail " + personDetail);
		
		//Speichern der Person in der Datenbank
		personRepo.save(personDetail);
		response.redirect("/persons");
		return null;
	}
}


