package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Risk;
import ch.briggen.bfh.sparklist.domain.RiskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für delete-Operation für Risiken

 * @author Dominique Bachmann
 *
 */

public class RiskDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RiskDeleteController.class);
		
	private RiskRepository riskRepo = new RiskRepository();
	/**
	 * Löscht die Person mit der übergebenen id in der Datenbank
	 * /person/delete&id=987 löscht die Person mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /reports/risk/project/delete
	 * 
	 * @return Redirect zurück zur Risikoliste des Projekts (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /reports/risk/project/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		
		Risk m = riskRepo.getById(longId);
		long pid = m.getProjectId();

		response.redirect("/reports/risk/project?id="+pid);
		riskRepo.delete(longId);
		
		return null;
	}
}


