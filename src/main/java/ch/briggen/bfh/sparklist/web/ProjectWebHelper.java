package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import spark.Request;

/**
 * Beinhaltet Parameter für Projekt-Objekt in der View
 * 
 * @author Stefan Burger
 *
 */
class ProjectWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectWebHelper.class);
	
	public static Project projectFromWeb(Request request)
	{
		ArrayList<Long> personIds = new ArrayList<>();
		
			String persons = request.queryParams("projectDetail.persons");
			log.debug("this is a person-" + persons + "-this");
			String[] personsArray;
			
			
			//Prüft Ausgewählte Personen zum Hinzufügen
			if(persons != null && persons != "" && persons.length()>0){
		      persons.replace(" ", "");
		      personsArray = persons.split(",");
		      
		      for (int i = 0; i < personsArray.length; i++) {
		        long pid = Long.parseLong(personsArray[i]);
		        personIds.add(pid);
		        
		      }
			} 
		
		return new Project(
				Long.parseLong(request.queryParams("projectDetail.id")),
				request.queryParams("projectDetail.name"),
				request.queryParams("projectDetail.description"),
				request.queryParams("projectDetail.startdate"),
				request.queryParams("projectDetail.enddate"),
				Double.parseDouble(request.queryParams("projectDetail.budget")),
				Double.parseDouble(request.queryParams("projectDetail.currentcosts")),
				request.queryParams("projectDetail.status"),
				personIds);
	}

}
