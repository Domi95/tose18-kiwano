package ch.briggen.bfh.sparklist.web;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für new-Operation im Budgetplan
 * @author Dominique Bachmann
 *
 */

public class BudgetNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BudgetNewController.class);
		
	private BudgetRepository budgetRepo = new BudgetRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Erstellt ein neuer Budget-Eintrag in der DB. Die id wird von der Datenbank erstellt 
	 * Hört auf POST /reports/budget/project/new
	 * 
	 * @return Redirect bleibt auf gleicher Seite (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Budget budgetDetail = BudgetWebHelper.budgetFromWeb(request);
		log.trace("POST /reports/budget/project/new mit budgetDetail " + budgetDetail);
		
		long id = budgetRepo.insert(budgetDetail);
		long pid = budgetDetail.getProjectId();
		
		Project p = projectRepo.getById(pid);
		Collection<Budget> cb = budgetRepo.getByProject(pid);
		Integer fullCosts = 0;
		
		//Updated Gesamtkosten in Projekttabelle
		for (Budget b : cb) {
	        	fullCosts += b.getValue();
	        }
		
		projectRepo.updateCostsFromBudget(projectRepo.getById(pid), fullCosts);
		
		
		//Redirected auf aktuelle Seite (daher Projekt ID)
		response.redirect("/reports/budget/project?id="+pid);
		return null;
	}
}


