package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für edit-Operationen auf einzelne Personen
 * 
 * @author Dominique Bachmann
 *
 */

public class PersonEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(PersonEditController.class);
	
	
	
	private PersonRepository personRepo = new PersonRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Person. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /persons/person/new)
	 * Wenn der id Parameter != 0 ist wird beim submitten des Formulars das Projekt mit der übergebenen id upgedated (Aufruf /persons/person/save)
	 * Hört auf GET /persons/person
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. In diesem Fall "personDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//Prüft bereits Vorhandensein der Person
		if(null == idString)
		{
			log.trace("GET /persons/person für INSERT mit id " + idString);
			model.put("postAction", "/persons/person/new");
			model.put("personDetail", new Person());

		}
		else
		{
			log.trace("GET /persons/person für UPDATE mit id " + idString);
			model.put("postAction", "/persons/person/update");
			
			Long id = Long.parseLong(idString);
			Person p = personRepo.getById(id);
			//Repräsentative Daten
			model.put("personDetail", p);
		}
		
		//das Template personDetailTemplate verwenden und dann "anzeigen".
		return new ModelAndView(model, "personDetailTemplate");
	}
	
	
	
}


