package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für new-Operation auf einzelne Projekte

 * @author Stefan Burger
 *
 */

public class ProjectNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectNewController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Erstellt ein neues Project in der DB. Die id wird von der Datenbank erstellt.
	 * 
	 * Hört auf POST /project/new
	 * 
	 * @return Redirect zurück zur Projektliste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		log.trace("POST /project/new mit projectDetail " + projectDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = projectRepo.insert(projectDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /
		response.redirect("/");
		return null;
	}
}


