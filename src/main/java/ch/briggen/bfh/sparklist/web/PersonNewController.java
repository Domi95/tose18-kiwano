package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für new-Operationen der Personen
 * 
 * @author Dominique Bachmann
 *
 */

public class PersonNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonNewController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	
	/**
	 * Erstellt eine neue Person in der DB. Die id wird von der Datenbank erstellt.
	 * 
	 * Hört auf POST /persons/person/new
	 * 
	 * @return Redirect zurück zur Personenliste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		log.trace("POST /persons/person/new mit personDetail " + personDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = personRepo.insert(personDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /person?id=432932
		response.redirect("/persons");
		return null;
	}
}


