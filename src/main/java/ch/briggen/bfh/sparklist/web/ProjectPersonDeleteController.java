package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.PersonRepository;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Projekte

 * @author Stefan Burger
 *
 */

public class ProjectPersonDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectPersonDeleteController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Löscht beziehung zwischen Person und Projekt in DB
	 * 
	 * Hört auf GET /project/person/delete
	 * 
	 * @return Redirect zurück zum Projekt (Aktualisieren)
	 */	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String personId = request.queryParams("personid");
		String projectId = request.queryParams("projectid");
		log.trace("GET /project/person/delete mit id " + personId);
		
		Long longPersonId = Long.parseLong(personId);
		Long longProjectId = Long.parseLong(projectId);
		projectRepo.deleteRelation(longProjectId, longPersonId);
		response.redirect("/project?id="+projectId);
		return null;
	}
}


