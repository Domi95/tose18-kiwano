package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Risk;
import ch.briggen.bfh.sparklist.domain.RiskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Risiken
 * @author Dominique Bachmann
 *
 */

public class RiskNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RiskNewController.class);
		
	private RiskRepository riskRepo = new RiskRepository();
	private ProjectRepository prep = new ProjectRepository();
	
	/**
	 * Erstellt ein neues Risiko in der DB. Die id wird von der Datenbank erstellt.
	 * 
	 * Hört auf POST reports/risk/project/new
	 * 
	 * @return Redirect zur Risikoliste eines Projekts (Aktualisieren)
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Risk riskDetail = RiskWebHelper.riskFromWeb(request);
		log.trace("POST /reports/risk/project/new mit riskDetail " + riskDetail);
		
		//Berechnet Risikofaktor, schreibt diesen in die DB
		if (riskDetail.getProbability() * (riskDetail.getSeverity() * 2) > 9){
			riskDetail.setRating(3);
		}
		else if (riskDetail.getProbability() * (riskDetail.getSeverity() *2) > 5){
			riskDetail.setRating(2);
		}
		else {
			riskDetail.setRating(1);
		}
		
		//insert gibt die von der DB erstellte id zurück.
		long id = riskRepo.insert(riskDetail);
		long pid = riskDetail.getProjectId();

		response.redirect("/reports/risk/project?id="+pid);
		return null;
	}
}


