package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für lösch-Operation im Budgetplan
 * @author Stefan Burger
 *
 */

public class BudgetDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BudgetDeleteController.class);
		
	private BudgetRepository budgetRepo = new BudgetRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	

	/**
	 * Löscht die Budgetaktion mit der übergebenen id in der Datenbank
	 * Hört auf GET /reports/budget/project/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /reports/budget/project/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		Budget b = budgetRepo.getById(longId);
		long pid = b.getProjectId();
		Project p = projectRepo.getById(pid);
		
		//Updated Kosten in Projekttabelle
		response.redirect("/reports/budget/project?id="+pid);
		projectRepo.updateCostsFromBudget(p, (int) (p.getCurrentCosts() - b.getValue()));
		budgetRepo.delete(longId);
		
		return null;
	}
}


