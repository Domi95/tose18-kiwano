package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.PersonDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonRepositorySQLErrorTest {

	PersonRepository personRepo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		personRepo = new PersonRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionGetAll() {
		assertThrows(RepositoryException.class, ()->{personRepo.getAll();});		
	}
	
	@Test
	void testSQLExceptionGetByName() {
		assertThrows(RepositoryException.class, ()->{personRepo.getByName("PersonThatDoesNotExist");});		
	}
	
	@Test
	void testSQLExceptionGetById() {
		assertThrows(RepositoryException.class, ()->{personRepo.getById(0);});		
	}
	
	@Test
	void testSQLExceptionInsert() {
		assertThrows(RepositoryException.class, ()->{personRepo.insert(null);});		
	}

	@Test
	void testSQLExceptionUpdate() {
		assertThrows(RepositoryException.class, ()->{personRepo.save(null);});		
	}
	
	@Test
	void testSQLExceptionDelete() {
		assertThrows(RepositoryException.class, ()->{personRepo.delete(0);});		
	}
}
