package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ProjectTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Project i = new Project();
		assertThat("Id",i.getId(),is(equalTo(0)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Description",i.getDescription(),is(equalTo(0)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testContructorAssignsAllFields(long id,String name, String description) {
		Project i = new Project(id, name, description, "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("Description",i.getDescription(),equalTo(description));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testSetters(long id,String name, String description) {
		Project i = new Project();
		i.setId(id);
		i.setName(name);
		i.setDescription(description);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("Description",i.getDescription(),equalTo(description));
	}
	
	@Test
	void testToString() {
		Project i = new Project();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
