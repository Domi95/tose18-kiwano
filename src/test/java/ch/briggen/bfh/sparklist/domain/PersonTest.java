package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PersonTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Person p = new Person();
		assertThat("Id",p.getId(),is(equalTo(0)));
		assertThat("Name",p.getName(),is(equalTo(null)));
		assertThat("Firstname",p.getFirstname(),is(equalTo(0)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testContructorAssignsAllFields(long id,String name, String firstname) {
		Person p = new Person(0,name,firstname,"Auftraggeber");
		assertThat("Id",p.getId(),equalTo(id));
		assertThat("Name",p.getName(),equalTo(name));
		assertThat("Firstname",p.getFirstname(),equalTo(firstname));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testSetters(long id,String name, String firstname) {
		Person p = new Person();
		p.setId(id);
		p.setName(name);
		p.setFirstname(firstname);
		assertThat("Id",p.getId(),equalTo(id));
		assertThat("Name",p.getName(),equalTo(name));
		assertThat("Firstname",p.getFirstname(),equalTo(firstname));
	}
	
	@Test
	void testToString() {
		Person p = new Person();
		System.out.println(p.toString());
		assertThat("toString smoke test",p.toString(),not(nullValue()));
	}
}
