package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.ProjectDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class ProjectRepositoryTest {

	ProjectRepository projectRepo = null;

	
	private static void populateRepo(ProjectRepository r) {
		for(int i = 0; i <10; ++i) {
			Project dummy = new Project(0, "Fake Test Project " + i,"Beschreibung", "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		projectRepo = new ProjectRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",projectRepo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(projectRepo);
		assertThat("Freshly populated DB must hold 10 entrys",projectRepo.getAll().size(),is(10));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1","4,Four,2","5,Five,3","6,Six,4","7,Seven,5"})
	void testInsertProjects(long id,String name, String description, String status, String startdate, String enddate,int budget, int currentcosts)
	{
		populateRepo(projectRepo);
		
		Project p = new Project(id,name, description, startdate, enddate, budget, currentcosts, status, null);
		long dbId = projectRepo.insert(p);
		Project fromDB = projectRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("description = description", fromDB.getDescription(),is(description) );
		assertThat("startdate = startdate", fromDB.getStartDate(),is(startdate) );
		assertThat("enddate = enddate", fromDB.getEndDate(),is(enddate) );
		assertThat("budget = budget", fromDB.getBudget(),is(budget) );
		assertThat("currentcosts = currentcosts", fromDB.getCurrentCosts(),is(currentcosts) );

	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1,Eins,-2","2,Two,0,Zwei,-1","3,Three,1,Drei,0"})
	void testUpdateProjects(long id,String name,String description,String newName,String newDescription)
	{
		populateRepo(projectRepo);
		
		Project p = new Project(id,name,description, "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
		long dbId = projectRepo.insert(p);
		p.setId(dbId);
		p.setName(newName);
		p.setDescription(newDescription);
		projectRepo.save(p);
		
		Project fromDB = projectRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(newName) );
		assertThat("description = description", fromDB.getDescription(),is(newDescription) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testDeleteProjects(long id,String name,String description)
	{
		populateRepo(projectRepo);
		
		Project p = new Project(id,name,description, "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
		long dbId = projectRepo.insert(p);
		Project fromDB = projectRepo.getById(dbId);
		assertThat("Project was written to DB",fromDB,not(nullValue()));
		projectRepo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{projectRepo.getById(dbId);},"Project should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(projectRepo);
		for(Project i : projectRepo.getAll())
		{
			projectRepo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",projectRepo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetByOneName(long id,String name,String description)
	{
		populateRepo(projectRepo);
		
		Project i = new Project(id,name,description, "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
		projectRepo.insert(i);
		Collection<Project> fromDB = projectRepo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Project elementFromDB = fromDB.iterator().next();
		
		assertThat("name = name", elementFromDB.getName(),is(name) );
		assertThat("description = description", elementFromDB.getDescription(),is(description) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetManyProjectsByName(int count,String name,String description)
	{
		populateRepo(projectRepo);
		
		
		for(int n = 0; n < count; ++n) {
			Project i = new Project(0,name,description, "01.01.2018", "31.12.2018", 1000, 1000, "Grün", null);
			projectRepo.insert(i);
		}
		
		Collection<Project> fromDB = projectRepo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(	Project elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("description = description", elementFromDB.getDescription(),is(description) );
		}
	}
	
	@Test
	void testGetNoProjectsByName()
	{
		populateRepo(projectRepo);
		
		Collection<Project> fromDB = projectRepo.getByName("NotExistingProject");
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
}
