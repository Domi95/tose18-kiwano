package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ProjectDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProjectRepositorySQLErrorTest {

	ProjectRepository projectRepo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		projectRepo = new ProjectRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionGetAll() {
		assertThrows(RepositoryException.class, ()->{projectRepo.getAll();});		
	}
	
	@Test
	void testSQLExceptionGetByName() {
		assertThrows(RepositoryException.class, ()->{projectRepo.getByName("ProjectThatDoesNotExist");});		
	}
	
	@Test
	void testSQLExceptionGetById() {
		assertThrows(RepositoryException.class, ()->{projectRepo.getById(0);});		
	}
	
	@Test
	void testSQLExceptionInsert() {
		assertThrows(RepositoryException.class, ()->{projectRepo.insert(null);});		
	}

	@Test
	void testSQLExceptionUpdate() {
		assertThrows(RepositoryException.class, ()->{projectRepo.save(null);});		
	}
	
	@Test
	void testSQLExceptionDelete() {
		assertThrows(RepositoryException.class, ()->{projectRepo.delete(0);});		
	}
}
