package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.PersonDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class PersonRepositoryTest {

	PersonRepository personRepo = null;

	
	private static void populateRepo(PersonRepository r) {
		for(int i = 0; i <10; ++i) {
			Person p = new Person(0,"Fake TestPerson","Fake TestVorname","Auftraggeber");
			r.insert(p);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		personRepo = new PersonRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",personRepo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(personRepo);
		assertThat("Freshly populated DB must hold 10 entrys",personRepo.getAll().size(),is(10));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testInsertPersons(long id,String name, String firstname, String status, String startdate, String enddate,int budget, int currentcosts)
	{
		populateRepo(personRepo);
		
		Person p = new Person(id,name, firstname, "Auftraggeber");
		long dbId = personRepo.insert(p);
		Person fromDB = personRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("firstname = firstname", fromDB.getFirstname(),is(firstname) );

	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1,Eins,-2","2,Two,0,Zwei,-1","3,Three,1,Drei,0"})
	void testUpdatePersons(long id,String name,String firstname,String newName,String newFirstname)
	{
		populateRepo(personRepo);
		
		Person p = new Person(0,name,firstname,"Auftraggeber");
		long dbId = personRepo.insert(p);
		p.setId(dbId);
		p.setName(newName);
		p.setFirstname(newFirstname);
		personRepo.save(p);
		
		Person fromDB = personRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(newName) );
		assertThat("firstname = firstname", fromDB.getFirstname(),is(newFirstname) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testDeletePersons(long id,String name,String firstname)
	{
		populateRepo(personRepo);
		
		Person p = new Person(0,name,firstname,"Auftraggeber");
		long dbId = personRepo.insert(p);
		Person fromDB = personRepo.getById(dbId);
		assertThat("Person was written to DB",fromDB,not(nullValue()));
		personRepo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{personRepo.getById(dbId);},"Person should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(personRepo);
		for(Person i : personRepo.getAll())
		{
			personRepo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",personRepo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetByOneName(long id,String name,String firstname)
	{
		populateRepo(personRepo);
		
		Person i = new Person(0,name,firstname,"Auftraggeber");
		personRepo.insert(i);
		Collection<Person> fromDB = personRepo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Person elementFromDB = fromDB.iterator().next();
		
		assertThat("name = name", elementFromDB.getName(),is(name) );
		assertThat("firstname = firstname", elementFromDB.getFirstname(),is(firstname) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testGetManyPersonsByName(int count,String name,String firstname)
	{
		populateRepo(personRepo);
		
		
		for(int n = 0; n < count; ++n) {
			Person i = new Person(0,name,firstname,"Auftraggeber");
			personRepo.insert(i);
		}
		
		Collection<Person> fromDB = personRepo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(	Person elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("firstname = firstname", elementFromDB.getRole(),is(firstname) );
		}
	}
	
	@Test
	void testGetNoPersonsByName()
	{
		populateRepo(personRepo);
		
		Collection<Person> fromDB = personRepo.getByName("NotExistingPerson");
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
}
